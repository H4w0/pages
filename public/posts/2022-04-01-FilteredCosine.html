<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=0.45">
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
	<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

<a class	="logo" href="../index.html"><img src=../assets/logos/haiku.jpg/></a>
<ul>
	<li><a href="../index.html"><strong>Arthur Cavalier</strong></a>
	<li>PhD and Research Engineer in Computer Graphics 
	<li>Mail: arthur.cavalier <strong>[at]</strong> univ-poitiers <strong>[dot]</strong> fr
	<li><a href="https://mastodon.gamedev.place/@acavalie">Mastodon</a> <a href="https://bsky.app/profile/acavalie.bsky.social">Bluesky</a> <a href="https://shadertoy.com/user/H4w0">Shadertoy</a>
	</ul>
<br/>
<title>Prefiltered Cosine</title>
	<h1>Prefiltered Cosine
</h1><p>Conventions:
<ul><li>I use the non-unitary fourier transform with the angular frequency notation (i.e. \(\omega = 2\pi\xi\))
<li>I use \(\mathcal{F}\) and \(\mathcal{F}^{-1}\) to represent fourier transform and its inverse as functions
<li>Performing a fourier transform yields a fourier pair \( f,\hat{f} \)
<li>\(N\) is the dimension
<li>\(\mathbf{x},\mathbf{f}\) are column vectors of dimension \(N\) in \(\mathbb{R}^N\)
</ul><p><h2>Objectives
</h2><p>We want to use a cosine function as a procedural texture:
<p>$$
f(\mathbf{x};\alpha,\mathbf{f}) = \alpha \cos(\mathbf{f}^T\mathbf{x})
$$
<p>where \(\alpha\) is the cosine amplitude and \(\mathbf{f}^T\mathbf{x}\) is a dot product between \(\mathbf{f}\) and \(\mathbf{x}\).
<p>However if we apply this procedural texture naively, we'll obtain aliasing.
We need to compute the "pre-filtered" cosine instead. It means we need to compute a band-limited version of this signal. 
<p><h2>Pixel footprint
</h2><p>I chose a gaussian pixel footprint. It is defined in texture space as:
<p>$$
\frac{1}{2\pi\sqrt{|JJ^T|}} \exp\big( -\frac{1}{2}  \mathbf{x}^T (JJ^{T})^{-1} \mathbf{x} \big)        
$$
<p>where J is the Jacobian of the texture mapping :
<p>$$
J = \sigma \begin{pmatrix} \frac{du}{dx} & \frac{du}{dy} \\ \frac{dv}{dx} & \frac{dv}{dy} \end{pmatrix}
$$
<p>By posing a centered gaussian function as:
<p>$$
g(\mathbf{x};\lambda,\Sigma) = \lambda \, \exp( -\frac{1}{2} \mathbf{x}^T\Sigma^{-1}\mathbf{x} )
$$
<p>where \(\lambda\) is the gaussian amplitude and \(\Sigma\) is the gaussian covariance matrix.
<p>We can rewrite the pixel footprint as:
<p>$$
g(\mathbf{x};\frac{1}{2\pi\sqrt{|JJ^T|}},JJ^T)
$$
<p><h2>Convolution and Fourier transform
</h2><p>To filter the cosine function \(f\) we need to compute the convolution \(g \circledast f\) within the projected gaussian pixel footprint \(g\).
<p>In spatial domain it yields:
<p>$$
(g \circledast f)(t) = \int g(\tau)f(t-\tau)d\tau
$$
<p>Due to the convolution theorem, we know that the Fourier transform of a convolution is the product of their Fourier transform, hence:
<p>$$
\mathcal{F}(g \circledast f) = \hat{g} * \hat{f}
$$
<p>It means we can chose to resolve the convolution in spatial domain (by doing the integral) or in the spectral domain (i.e. computing the inverse transform of the product of the transforms).
<p><h2>Fourier transform of a gaussian
</h2><p>$$
\begin{aligned}
\mathcal{F}\big( g(\mathbf{x};\lambda,\Sigma) \big) &= \int_{\mathbb{R}^N} g(\mathbf{x};\lambda,\Sigma) e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} \\
&= \lambda \int_{\mathbb{R}^N} e^{ -\frac{1}{2} \mathbf{x}^T\Sigma^{-1}\mathbf{x} } e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} \\
&= \lambda (2\pi)^{N/2} |\Sigma|^{1/2} e^{-\frac{1}{2} \omega^T\Sigma \,\omega} \\
&= \hat{g}(\omega;\lambda (2\pi)^{N/2} |\Sigma|^{1/2},\Sigma^{-1})
\end{aligned}
$$
<p><details>
<summary> Proof: Inverse Fourier transform (<em>click</em>)</summary>
<p>
<p>$$
\begin{aligned}
\mathcal{F}^{-1}\Big( \hat{g}(\omega;\lambda (2\pi)^{N/2} |\Sigma|^{1/2},\Sigma^{-1}) \big) \Big) &=
\frac{1}{(2\pi)^N}\int_{\mathbb{R}^N} g(\omega;\lambda (2\pi)^{N/2} |\Sigma|^{1/2},\Sigma^{-1}) e^{i\omega^T\mathbf{x}}\;d\omega \\
&= \frac{\lambda (2\pi)^{N/2} |\Sigma|^{1/2}}{(2\pi)^N} \int_{\mathbb{R}^N}
e^{-\frac{1}{2} \omega^T\Sigma \,\omega} e^{i\omega^T\mathbf{x}} \; d\omega \\
&= \frac{\lambda (2\pi)^{N/2} |\Sigma|^{1/2}}{(2\pi)^N} (2\pi)^{N/2} |\Sigma^{-1}|^{1/2} e^{-\frac{1}{2} \mathbf{x}^T\Sigma^{-1} \,\mathbf{x}} \\
&= \lambda e^{ -\frac{1}{2} \mathbf{x}^T\Sigma^{-1}\mathbf{x} } \\
&= g(\mathbf{x};\lambda,\Sigma)
\end{aligned}
$$
<p></p>
</details>
<p><p><details>
<summary> Integral of a gaussian (<em>click</em>)</summary>
<p>
<p>$$
\int_{\mathbb{R}^N} g(\mathbf{x};\lambda,\Sigma) \; d\mathbf{x} = \lambda (2\pi)^{N/2} |\Sigma|^{1/2}
$$
<p><p></p>
</details>
<p>these integrals are simplified by rearranging squared-forms in exponentials.
<p><p><h2>Fourier transform of a cosine
</h2><p>The Fourier transform a cosine requires knowledge of the dirac delta properties:
<details>
<summary>Dirac delta properties (<em>click</em>)</summary>
<p>
<p><ul><li>it's an even distribution:
$$
\delta(x) = \delta(-x)
$$
</ul><p><ul><li>its sifting properties:
$$
\begin{aligned}
\int f(x)\delta(x)dx &= f(0) \\
\int f(\tau)\delta(t-\tau)d\tau &= f(t) \\
\end{aligned}
$$
</ul><p><ul><li>its fourier transform:
</ul><p>$$
\begin{aligned}
\mathcal{F}\big(\delta(\mathbf{x})\big) &= \int_{\mathbb{R}^N} \delta(\mathbf{x})e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} = 1 \\
\mathcal{F}^{-1}\big(1\big) &= \frac{1}{(2\pi)^N}\int_{\mathbb{R}^N} e^{i\omega^T\mathbf{x}}\;d\omega = \delta(\mathbf{x})
\end{aligned}
$$
<p></p>
</details>
<p>To compute the fourier transform, we will need to use the Euler's formula:
$$
f(\mathbf{x};\alpha,\mathbf{f}) = \alpha \cos(\mathbf{f}^T\mathbf{x}) = \alpha \frac{ e^{-i\mathbf{f}^T\mathbf{x}} + e^{i\mathbf{f}^T\mathbf{x}} }{2}
$$
<p>Hence:
$$
\begin{aligned}
\hat{f}(\omega;\alpha,\mathbf{f}) &= \mathcal{F}\big( f(\mathbf{x};\alpha,\mathbf{f}) \big)\\
&= \alpha \int_{\mathbb{R}^N} \cos(\mathbf{f}^T\mathbf{x})e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} \\
&= \frac{\alpha }{2} \int_{\mathbb{R}^N} (e^{-i\mathbf{f}^T\mathbf{x}} + e^{i\mathbf{f}^T\mathbf{x}}) e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} \\
&= \frac{\alpha }{2} \Big[\int_{\mathbb{R}^N} e^{-i\mathbf{f}^T\mathbf{x}}e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} + \int_{\mathbb{R}^N} e^{i\mathbf{f}^T\mathbf{x}}e^{-i\omega^T\mathbf{x}}\;d\mathbf{x} \Big] \\
&= \frac{\alpha }{2} \Big[\int_{\mathbb{R}^N} e^{i\omega^T(\mathbf{x}-\mathbf{f})}\;d\mathbf{x} + \int_{\mathbb{R}^N} e^{i\omega^T(\mathbf{x}+\mathbf{f})}\;d\mathbf{x} \Big] \\
&=  \frac{\alpha (2\pi)^N}{2} \Big[ \delta(\omega-\mathbf{f}) + \delta(\omega+\mathbf{f}) \Big] \\
\end{aligned}
$$
<p><details>
<summary> Proof: Inverse Fourier transform (<em>click</em>)</summary>
<p>
<p>$$
\begin{aligned}
\mathcal{F}^{-1} \Big( \hat{f}(\omega;\alpha,\mathbf{f}) \Big)&= 
\frac{\alpha }{(2\pi)^N}\int_{\mathbb{R}^N} \frac{(2\pi)^N}{2} \Big[ \delta(\omega-\mathbf{f}) + \delta(\omega+\mathbf{f}) \Big] e^{i\omega^T\mathbf{x}}\;d\omega \\
&= \frac{\alpha }{2}\int_{\mathbb{R}^N} \Big[ \delta(\omega-\mathbf{f}) + \delta(\omega+\mathbf{f}) \Big] e^{i\omega^T\mathbf{x}}\;d\omega \\
&= \frac{\alpha }{2} \Big[ \int_{\mathbb{R}^N} e^{i\omega^T\mathbf{x}} \delta(\omega-\mathbf{f}) \;d\omega + \int_{\mathbb{R}^N} e^{i\omega^T\mathbf{x}} \delta(\omega+\mathbf{f}) \;d\omega \Big] \\
&= \frac{\alpha }{2} \Big[ e^{-i\mathbf{f}^T\mathbf{x}} + e^{i\mathbf{f}^T\mathbf{x}} \Big] \\
&= \alpha \cos(\mathbf{f}^T\mathbf{x}) \\
&= f(\mathbf{x};\alpha,\mathbf{f})
\end{aligned}
$$
<p></p>
</details>
<p><h2>Prefiltering cosine
</h2><p>We compute the convolution \(g \circledast f\) in the spectral domain:
<p>$$
g \circledast f = \mathcal{F}^{-1}( \mathcal{F}(g \circledast f)) = \mathcal{F}^{-1}( \hat{g}*\hat{f} )
$$
<p>Hence:
<p>$$
\begin{aligned}
\mathcal{F}^{-1}( \hat{g}*\hat{f} ) &= \frac{1}{(2\pi)^N} \int_{\mathbb{R}^N}   \frac{\alpha (2\pi)^N}{2} \Big[ \delta(\omega-\mathbf{f}) + \delta(\omega+\mathbf{f}) \Big] g(\omega;\lambda (2\pi)^{N/2} |\Sigma|^{1/2},\Sigma^{-1}) e^{i\omega^T\mathbf{x}}\;d\omega \\
&= \frac{\lambda (2\pi)^{N/2} |\Sigma|^{1/2}}{(2\pi)^N} \frac{\alpha (2\pi)^N}{2} \int_{\mathbb{R}^N} \Big[ \delta(\omega-\mathbf{f}) + \delta(\omega+\mathbf{f}) \Big] e^{-\frac{1}{2} \omega^T\Sigma \,\omega} e^{i\omega^T\mathbf{x}}\;d\omega \\
&= \big( \lambda (2\pi)^{N/2} |\Sigma|^{1/2} \big) \frac{\alpha}{2} \Big[ \int_{\mathbb{R}^N} e^{-\frac{1}{2} \omega^T\Sigma \,\omega +i\omega^T\mathbf{x}} \delta(\omega-\mathbf{f})\;d\omega + \int_{\mathbb{R}^N} e^{-\frac{1}{2} \omega^T\Sigma \,\omega +i\omega^T\mathbf{x}} \delta(\omega+\mathbf{f})\;d\omega\Big] \\
&= \big( \lambda (2\pi)^{N/2} |\Sigma|^{1/2} \big) \frac{\alpha}{2}  \Big[ e^{-\frac{1}{2} \mathbf{f}^T\Sigma \,\mathbf{f}} e^{i\mathbf{f}^T\mathbf{x}} + e^{-\frac{1}{2} \mathbf{f}^T\Sigma \,\mathbf{f}} e^{-i\mathbf{f}^T\mathbf{x}} \Big] \\
&= \big( \lambda (2\pi)^{N/2} |\Sigma|^{1/2} e^{-\frac{1}{2} \mathbf{f}^T\Sigma \,\mathbf{f}} \big) \frac{\alpha}{2} \Big[ e^{i\mathbf{f}^T\mathbf{x}} + e^{-i\mathbf{f}^T\mathbf{x}} \Big] \\
&= \Big( \lambda (2\pi)^{N/2} |\Sigma|^{1/2} e^{-\frac{1}{2} \mathbf{f}^T\Sigma \,\mathbf{f}} \Big) \; \alpha \cos(\mathbf{f}^T\mathbf{x}) \\
&= g \circledast f
\end{aligned}
$$
<p>By substituting the gaussian footprint covariance matrix and amplitude to last equation, the prefiltered 2D cosine function is:
<p>$$
\exp(-\frac{1}{2} \mathbf{f}^T JJ^{T} \,\mathbf{f}) \; \alpha \cos(\mathbf{f}^T\mathbf{x})
$$
<p>All this work to lead to this tiny glsl implementation:
<p><pre>float filtered_cosine(vec2 x, float alpha, vec2 f)
{
    mat2  J = 0.5 * mat2(dFdx(x),dFdy(x));
    mat2  S = J * transpose(J);
    return alpha * cos( dot(x,f) ) * exp(-0.5*dot(f,S*f)); 
}
</pre><p>Here you can see it in action, with \(\mathbf{f}\) as a 2D column vector defined as \(2\pi F_0 (cos(\theta_0),sin(\theta_0))^T\) where \(F_0\) the frequency, \(\theta_0\) is the orientation. 
<p><p><p><div align="center"><iframe width="640" height="360" frameborder="0" src="https://www.shadertoy.com/embed/NtlcWf?gui=true&t=10&paused=true&muted=false" allowfullscreen></iframe></div>
<p><p>BONUS: It can be demonstrated that a phase-shifted cosine yields the same results (hint: a common factor \(e^{i\phi}\) multiplies all integral).</body>
</html>
